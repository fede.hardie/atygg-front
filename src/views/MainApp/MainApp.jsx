import React, { useEffect, useReducer } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppRouter from '../../router/AppRouter';
import { UserContext } from '../../context/UserContext';
import { authReducer } from '../../reducers/authReducer';

const init = () => {
  return JSON.parse(window.localStorage.getItem('user')) || { logged: false };
}

const MainApp = () => {
  const [jwt, dispatch] = useReducer(authReducer, {}, init);

  useEffect(() => {
      localStorage.setItem('user', JSON.stringify(jwt));
  }, [jwt])

  return (
    <UserContext.Provider value={{ jwt, dispatch }}>
      <AppRouter />
    </UserContext.Provider>
  );
};

export default MainApp;
