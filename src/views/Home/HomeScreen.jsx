import React from 'react'
import { NavbarComponent } from '../../components/Navbar';
import { TableUser } from '../../components/User';

const HomeScreen = () => {
    return (
        <>
            <div className="container-fluid">
                <TableUser />
            </div>
        </>
    );
}

export default HomeScreen;
