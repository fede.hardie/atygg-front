import axios from 'axios';

const tokenizedPost = (URL, data, token) => {
    return axios.post(`${process.env.REACT_APP_API}/${URL}`, data, { headers: { "Authorization": `Bearer ${token}` } });
}

const postWithoutToken = (URL, data) => {
    return axios.post(`${process.env.REACT_APP_API}/${URL}`, data);
}

export {
    tokenizedPost,
    postWithoutToken
}