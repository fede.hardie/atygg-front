import { types } from '../../reducers/types';

const logout = ({ dispatch }) => {
    dispatch({
        type: types.logout
    });
};

export default logout;