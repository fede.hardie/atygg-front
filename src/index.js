import React from 'react';
import ReactDOM from 'react-dom';
import MainApp from './views/MainApp';

ReactDOM.render(
  <MainApp />,
  document.getElementById('root'),
);