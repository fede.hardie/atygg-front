import {
    useState,
} from 'react';

export const useForm = ({ initialState }) => {

    const [values, setValues] = useState(initialState);

    const handleChange = (event) => {
        const { name, value } = event.target;
        setValues(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    return [values, handleChange, setValues];
};