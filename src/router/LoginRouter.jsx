import React, { useContext } from 'react';
import { Navigate, Outlet } from "react-router-dom";
import { UserContext } from '../context/UserContext';

const LoginRouter = () => {
    const { jwt } = useContext(UserContext);

    return (!jwt.logged) ? <Outlet /> : <Navigate to="/" />;
};

export default LoginRouter;