import React from "react";
import {
    BrowserRouter as Router,
    Routes,
    Route
} from "react-router-dom";
import HomeScreen from "../views/Home";
import LoginScreen from "../views/Login";
import LoginRouter from "./LoginRouter";
import PrivateRouter from "./PrivateRouter";

const AppRouter = () => {
    return (
        <Router>
            <Routes>
                <Route exact path='/login' element={<LoginRouter />}>
                    <Route exact path='/login' element={<LoginScreen />} />
                </Route>

                <Route exact path='/' element={<PrivateRouter />}>
                    <Route exact path='/' element={<HomeScreen />} />
                </Route>
            </Routes>
        </Router>
    );
};

export default AppRouter;