import React, { useContext } from 'react';
import { Navigate, Outlet } from "react-router-dom";
import { NavbarComponent } from '../components/Navbar';
import { UserContext } from '../context/UserContext';

const PrivateRouter = () => {
    const { jwt } = useContext(UserContext);

    return (jwt.logged)
        ?
            <>
                <NavbarComponent />
                <Outlet />
            </>
        :
        <Navigate to="/login" />;
};

export default PrivateRouter;