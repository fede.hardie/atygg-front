import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useContext } from 'react'
import { Nav, Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { UserContext } from '../../context/UserContext';
import logout from '../../services/Login/logout';
import './styles.css';

const NavbarComponent = () => {
    const { jwt, dispatch } = useContext(UserContext);

    const handleLogout = () => {
        logout({ dispatch });
    }
    return (
        <>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Navbar.Brand>
                    <Link to="/" className="color-navbar">ATyGG</Link>
                </Navbar.Brand>
                <Navbar.Collapse className="justify-content-end">
                    
                    <Nav>
                        <Link to="/login" className="color-navbar" onClick={handleLogout}>
                            <FontAwesomeIcon className="icon-navbar-danger" icon={faSignOutAlt} />&nbsp;&nbsp;Cerrar sesión
                        </Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
            <div className="container-bottom">
                <div className="container-welcome">
                    Bienvenido {jwt.user.name} {jwt.user.lastname}
                </div>
            </div>
        </>
    );
}

export default NavbarComponent;
