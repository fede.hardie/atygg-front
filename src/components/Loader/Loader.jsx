import React from 'react'
import { Spinner } from 'react-bootstrap';
import './styles.css';

const Loader = () => {

    return (
        <>
            <div className="container-loader">
                <Spinner animation="grow" />
            </div>
        </>
    );
}

export default Loader;


