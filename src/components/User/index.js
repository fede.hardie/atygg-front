import DeleteUser from "./DeleteUser";
import FormUser from "./FormUser";
import ModalUser from "./ModalUser";
import TableUser from "./TableUser";

export {
    DeleteUser,
    FormUser,
    ModalUser,
    TableUser,
};
